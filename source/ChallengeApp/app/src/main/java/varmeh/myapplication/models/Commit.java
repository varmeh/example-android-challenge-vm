package varmeh.myapplication.models;

import com.google.gson.annotations.SerializedName;

public class Commit {

    private String sha;

    @SerializedName("commit")
    private CommitDetails commitDetails;

    public String getSha() {
        return sha;
    }

    public String getAuthorName(){

        if(commitDetails == null){
            return "";
        }

        return commitDetails.getAuthor().getName();
    }

    public String getMessage(){

        if(commitDetails == null){
            return "";
        }

        return commitDetails.getMessage();
    }
}
