package varmeh.myapplication.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import varmeh.myapplication.models.Commit;

public interface GithubService {

    @GET("repos/{owner}/{repo}/commits")
    Call<List<Commit>> listRepos(@Path("owner") String owner, @Path("repo") String repo);
}

