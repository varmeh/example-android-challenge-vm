package varmeh.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import varmeh.myapplication.interfaces.GithubService;
import varmeh.myapplication.models.Commit;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String GITHUB_URL = "https://api.github.com";
    public static final String GITLAB_REPO_OWNER = "square";
    public static final String GITLAB_REPO_NAME = "retrofit";

    @BindView(R.id.commitList) RecyclerView mCommitList;
    @BindView(R.id.loadingView) View mLoadingView;
    @BindView(R.id.progress) ProgressBar mProgress;
    @BindView(R.id.message) TextView mMessage;

    private CommitAdapter mCommitAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Setup the RecycleView
        mCommitAdapter = new CommitAdapter(new ArrayList<Commit>(0));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mCommitList.setLayoutManager(layoutManager);
        mCommitList.setAdapter(mCommitAdapter);
        mCommitList.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        mCommitList.addItemDecoration(itemDecoration);

        // Grab the commits from Github
        fetchCommits();
    }

    /**
     * Fetch all of the commits from Github for the Retrofit repo
     */
    private void fetchCommits(){

        Log.d(TAG, "Fetching Commits");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GITHUB_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GithubService githubService = retrofit.create(GithubService.class);

        githubService.listRepos(GITLAB_REPO_OWNER, GITLAB_REPO_NAME).enqueue(new Callback<List<Commit>>() {
            @Override
            public void onResponse(Call<List<Commit>> call, Response<List<Commit>> response) {

                if(response.isSuccessful()) {

                    Log.d(TAG, "Commits successfully fetched");

                    mLoadingView.setVisibility(View.GONE);

                    // Sort the results by name, and then update the RecycleView
                    Collections.sort(response.body(), new Comparator<Commit>() {
                        @Override
                        public int compare(Commit commit2, Commit commit1){
                            return commit2.getAuthorName().toLowerCase().compareTo(commit1.getAuthorName().toLowerCase());
                        }
                    });

                    mCommitAdapter.updateList(response.body());
                }else {

                    int statusCode  = response.code();
                    Log.d(TAG, "Commits fetch error " + statusCode);
                    showError();
                }
            }

            @Override
            public void onFailure(Call<List<Commit>> call, Throwable t) {
                Log.d(TAG, "Commits fetch error", t);
                showError();
            }
        });

    }

    private void showError(){

        mProgress.setVisibility(View.GONE);
        mMessage.setText(R.string.load_error);
    }

    public class CommitAdapter extends RecyclerView.Adapter<CommitAdapter.ViewHolder> {

        private List<Commit> mItems;

        public CommitAdapter( List<Commit> posts) {
            mItems = posts;
        }

        @Override
        public CommitAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
            View postView = inflater.inflate(R.layout.row_commit, parent, false);
            ViewHolder viewHolder = new ViewHolder(postView);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(CommitAdapter.ViewHolder holder, int position) {

            Commit repo = mItems.get(position);
            holder.person.setText(repo.getAuthorName());
            holder.commit.setText(getString(R.string.commit_placeholder).replaceFirst("%",repo.getSha()));
            holder.message.setText(repo.getMessage());
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public void updateList(List<Commit> items) {
            mItems = items;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            public TextView person;
            public TextView commit;
            public TextView message;

            public ViewHolder(View view) {
                super(view);
                person = (TextView) view.findViewById(R.id.person);
                commit = (TextView) view.findViewById(R.id.commit);
                message = (TextView) view.findViewById(R.id.message);
            }
        }
    }
}
