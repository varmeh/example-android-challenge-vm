package varmeh.myapplication.models;

public class CommitDetails {

    private Author author;
    private String message;

    public Author getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

}
